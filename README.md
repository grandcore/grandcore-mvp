# МВП платформы GrandCore

Это альтернативная реализация мвп. Основная ветка здесь https://gitlab.com/grandcore/grandcore-backend-python

## Ссылки:

- Подробнее о проекте: https://grandcore.org
- Экраны для мвп и концепты: https://cli.co/grandcore-figma
- ТЗ для мвп: https://cli.co/grandcore-tz
- Карта БД для мвп: https://cli.co/grandcore-db

## Установка на локальной машине:

_Выполните последовательно команды указанные ниже._

**Python**

см. [здесь](https://tutorial.djangogirls.org/ru/python_installation/)

**Nodejs/npm**

см. [здесь](https://nodejs.org/ru/download/package-manager/)

**SQLite**

```
pip3 install pysqlite3
```

**Django**

```
pip3 install django
```

## Запуск на локальной машине:

**Перейти в папку проекта**

```
cd path/to/project/folder
```

**Установить зависимости из package.json**

```
npm install
```

**Накатите миграции**

```
python3 manage.py migrate
```

**Запустить локальный сервер**

```
python3 manage.py runserver 8000
```

**Запустить сборку фронтенда**

```
gulp
```

_Вы готовы стать отцом проекта!_
