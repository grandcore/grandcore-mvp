

from django.contrib import admin
from django.urls import path
from default import views

from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
    
    path('registration/',views.registration, name="registration"),
    path('login/',views.login, name="login"),    
    path('reminder/',views.reminder, name="reminder"),
    path('',views.index, name="index"),
    path('about/',views.about, name="about"),
    path('new_project/',views.new_project, name="new_project"),
    path('donat/',views.donat, name="donat"),
    path('users/',views.users, name="users"),
    path('user/',views.user, name="user"),
    path('profile/',views.profile, name="profile"),
    path('projects/',views.projects, name="projects"),
    path('project/',views.project, name="project"),
    path('not_found/',views.not_found, name="not_found"),
 
]