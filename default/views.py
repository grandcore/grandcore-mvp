from django.shortcuts import render

def registration(request):
    return render(request,'default/registration.html')

def login(request):
    return render(request,'default/login.html')

def reminder(request):
    return render(request,'default/reminder.html')    

def index(request):
    return render(request,'default/index.html')

def about(request):
    return render(request,'default/about.html')    

def new_project(request):
    return render(request,'default/new_project.html')

def donat(request):
    return render(request,'default/donat.html')

def users(request):
    return render(request,'default/users.html')    

def user(request):
    return render(request,'default/user.html')

def profile(request):
    return render(request,'default/profile.html')

def projects(request):
    return render(request,'default/projects.html')  

def project(request):
    return render(request,'default/project.html') 

def not_found(request):
    return render(request,'default/not_found.html')    




